###Assignment 1


* git init - this command creats an empty Git repository - basically a .git directory.
* git status - displays the state of the working directory and the staging area.
* git add - adds all modified and new files in the current directory and all subdirectories to the staging area.
* git commit - records changes to the repository while git push updates remote refs.
* git push - used to push commits made on your local branch to a remote repository
* git pull - allows you to let others know about changes you've pushed to a Git repository.
* git clone - used to target an existing repository and create a clone of it.

####Screenshots

running *TipCalulator* using IDLE
![Tips](img/tipsIDLE.png)

running *TipCalulator* using Visual Studio Code
![Tips](img/tipsVS.png)

[Myteamquotes ](https://bitbucket.org/djb16c/myteamquotes)
  
[Station Locations ](https://bitbucket.org/djb16c/bitbucketstationlocations)
