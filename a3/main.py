import functions as f

def main():
        f.get_requirements()
        f.estimate_painting_cost()
        loop = raw_input("\nEstimate another paint job? (y/n): ")
        while loop == 'y':
                f.estimate_painting_cost()
                loop = raw_input("\nEstimate another paint job? (y/n): ")
        else:
                print("\nThank you for using our Painting Estimator!\nPlease see our web site: http://www.mysite.com")

main()