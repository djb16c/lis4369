def get_requirements():
    print("Painting Estimator\n")
    print("Program Requirements")
    print("1. Calculate home interior paint cost (w/o primer).")
    print("2. Must use float data types.")
    print("3. Must use SQFT_PER_GALLON constant (350).")
    print("4. Must use iteration structure (aka 'loop').")
    print("5. Format, right-align numbers, and round to two decimal places.")
    print("6. Create at least three functions that are called by the program:\n"
        + "\ta. main(): calls at least two other functions.\n"
        + "\tb. get_requirements(): displays the program requirements.\n"
        + "\tc. estimate_painting_cost(): calculates interior home painting.")

def estimate_painting_cost():
    SQFT_PER_GALLON = 350

    print("\nInput:")
    feet = float(input("Enter total interior sq ft: "))
    ppg = float(input("Enter price per gallon paint: "))
    rate = float(input("Enter hourly painting rate per sq ft: "))
    
    gal_amt = feet / SQFT_PER_GALLON
    paint_cost = ppg * gal_amt
    labor_cost = feet * rate
    total_cost = paint_cost + labor_cost
    paint_per = (paint_cost / total_cost) * 100
    labor_per = (labor_cost / total_cost) * 100
    total_per = (total_cost / total_cost) * 100
    
    print("\nOutput:")
    print("Item: \t\t\tAmount")
    print("Total Sq Ft: \t      " + str('{:,.2f}'.format(feet)))
    print("Sq Ft per Gallon: \t" + str('{:,.2f}'.format(SQFT_PER_GALLON)))
    print("Number of Gallons: \t  " + str('{:,.2f}'.format(gal_amt)))
    print("Paint per Gallon:    $\t " + str('{:,.2f}'.format(ppg)))
    print("Labor per Sq Ft:     $\t  " + str('{:,.2f}'.format(rate)))

    print("\nCost\t\tAmount\t   Percentage")
    print("Paint:\t     $  " + str('{:,.2f}\t\t'.format(paint_cost)) + str('{:,.2f}%'.format(paint_per)))
    print("Labor:\t     " + str('${:,.2f}\t       '.format(labor_cost)) + str('{:,.2f}%'.format(labor_per)))
    print("Total:\t     " + str('${:,.2f}\t      '.format(total_cost)) + str('{:,.2f}%'.format(total_per)))



