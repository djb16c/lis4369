#David Bohnsack
##LIS4369 Extensible Enterprise Solutions

####Course Work Links
* [Assignment 1](a1/README.md)  
a. Install Python  
b. Install R  
c. Install R studio  
d. Install Visual Studio Code  
e. Provide screenshots of installations  
f. Create Bitbucket repo  
g. Complete Bitbucket tutorial  
h. Provide git command descriptions 
* [Assignment 2](a2/README.md)  
a.  Use float data type for user input  
b.  Formart currenct with dollar sign  
c.  Create three functions that are called by the program  

* [Assignment 3](a3/README.md)  
a. Using iteration structure  
b. Create three functions that are called by this program  
c. Format, right-align numbers, and round to two decimal places
* [Assignment 4](a4/README.md)  
a. Test Python Package Installer: pip freeze  
b. Install pandas, pandas-datareader, and matplotlib  
c. Basic data scraping 

* [Assignment 5](a5/README.md)  

* [Project 1](p1/README.md)   
a. Test Python Package Installer: pip freeze  
b. Install pandas, pandas-datareader, and matplotlib  
c. Basic data scraping 

* [Project 2](p2/README.md)   
a. Use read.csv() in order to get information from URL  
b. Use summary() fucntion in order to get min, max, mean, median, and quantiles.   
c. Use ggplot2 to display graphs
